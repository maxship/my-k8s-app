В репозитории содержится [приложение](./index.php), отдающее страницу c IP адресом пода в кластере Кубернетеса, к которому было осуществлено подключение в данный момент.

Приложение собирается с помощью [Dockerfile](./Dockerfile).

Для развертывания в кластере Кубернетеса используется helm. Основные манифасты: [deployment.yaml](./deploy/templates/deployment.yaml), [service.yaml](./deploy/templates/service.yaml).

Пайплайны для сборки и деплоя описаны в [.gitlab-ci.yml](./.gitlab-ci.yml)


